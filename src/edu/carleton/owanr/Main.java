package edu.carleton.owanr;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    /**
     * Takes a string and determines whether it represents input ot output.
     * In particular, if the string starts with "--input", the method returns "input".
     * If the string starts with "--output", the method returns "output".
     * Returns an error otherwise
     *
     * @return "input", "output", or error
     */
    public static String detectInOrOut(String string) {
        String type = "";
        string = string.replaceAll("\\s", "");
        if (string.startsWith("--inputtype=")) {
            type = "input";
        } else if (string.startsWith("--outputtype=")) {
            type = "output";
        } else {
            //System.out.println(string);
            System.err.println("Usage: java EncodingHelper [--inputtype=type] [--outputtype=type] [data]");
            System.exit(1);
        }
        return type;
    }

    /**
     * Takes a code point and converts it to its string representation
     *
     * @return string
     */
    public static String codePointToString(int codePoint) {
        String returnVal = "";
        String toString = String.valueOf(Character.toChars(codePoint));
        try {
            byte[] converted = toString.getBytes("UTF-8");
            returnVal = new String(converted, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            System.out.println("Unsupported Character Set");
        }
        return returnVal;
    }

    /**
     * Checks if the user followed the usage statement with correct input or output comments.
     *
     * @param inOrOutAndType
     */
    public static void checkTypes(String inOrOutAndType) {
        String[] types = {"--inputtype=string", "--inputtype=utf8", "--inputtype=codepoint",
                "--outputtype=summary", "--outputtype=string", "--outputtype=utf8", "--outputtype=codepoint"};
        if (!Arrays.asList(types).contains(inOrOutAndType.replaceAll("\\s", ""))) {
            //System.out.println(inOrOutAndType);
            System.err.println("Usage: java EncodingHelper [--inputtype=type] [--outputtype=type] [data]");
            System.exit(1);
        }
    }



    /*
    **Creates and returns a list of EncodingHelperChar objects for words
     */
    public static List<EncodingHelperChar> createObjectList(String string, String input) {
        List<EncodingHelperChar> ehList = new ArrayList<EncodingHelperChar>();
        if (input.equals("multi-char string")
                || input.equals("one-char string")) {
            for (int a = 0; a < string.length(); a++) {
                EncodingHelperChar obj = new EncodingHelperChar(string.charAt(a));
                ehList.add(obj);
            }
        } else if (input.equals("utf8")) {
            //Change to codepoint first so we know how many characters

            /*
        String toString = String.valueOf(Character.toChars(this.codePoint));
        String toUTF8String = "";
        try {
            byte[] converted = toString.getBytes("UTF-8");
            //Constructs the hexadecimal byte string.
            for (byte b : converted) {
                toUTF8String = toUTF8String+"\\x"+String.format("%x", b).toUpperCase();
            }
        } catch (UnsupportedEncodingException e) {
            System.out.println("Unsupported Character Set");
        }
        return toUTF8String;*/
            String[] bytes = string.split(" ");
            for (int a = 0; a < string.length(); a++) {
                EncodingHelperChar obj = new EncodingHelperChar((byte) bytes[a].getBytes()[a]);
                ehList.add(obj);
            }
        } else if (input.equals("codepoint")) {
            String temp = string.replaceAll("\\s+", "");
            String[] codeP = string.split("U+");
            for (int a = 0; a < string.length(); a++) {
                Integer hexa = Integer.parseInt(codeP[a], 16);
                EncodingHelperChar obj = new EncodingHelperChar(hexa);
                ehList.add(obj);
            }
        }
        return ehList;
    }

    /**
     * Takes EncodingHelperChar[] and converts it to a word string
     */
    public static String getWordFromCP(List<EncodingHelperChar> ehList) {
        String string = "";
        for (int i = 0; i < ehList.size(); i++) {
            //byte[] b = new String({ehList.get(i).getCodePoint()});
            string = string + ehList.get(i).toString();
        }
        return string;
    }

    public static void printStringSummary(String string, String input) {
        System.out.println("String: " + string);
        System.out.print("Code points: ");
        for (int cha = 0; cha < string.length(); cha++) {
            System.out.print(createObjectList(string, input).get(cha).toCodePointString() + " ");
        }
        System.out.println("");
        System.out.print("UTF-8: ");
        for (int cha2 = 0; cha2 < string.length(); cha2++) {
            System.out.print(createObjectList(string, input).get(cha2).toUTF8StringWithoutQuotes() + " ");
        }
        System.out.println("");
    }

    public static String whatType(String string) {
        String whatType = "string";
        if (string.endsWith("utf8")) {
            whatType = "utf8";
        } else if (string.endsWith("point")) {
            whatType = "codepoint";
        } else if (string.endsWith("summary")) {
            whatType = "summary";
        }
        return whatType;
    }

    public static byte[] ifUTF8(String utf8) {
        utf8 = utf8.replaceAll("/x", "");
        byte[] temp = new byte[(utf8.length() / 2)];
        for (int i = 0; i < utf8.length() - 1; i += 2) {
            String output = utf8.substring(i, (i + 2));
            int decimal = Integer.parseInt(output, 16);
            temp[i / 2] = (byte) decimal;
        }
        return temp;
    }

    /*java EncodingHelper [--inputtype=type] [--outputtype=type] [data]
        inputtype can be any of: string [default], utf8, codepoint
        outputtype can be any of: summary [default], string, utf8, codepoint
        If the data argument is present, it is used as the input. Otherwise, input is taken from standard input.
         */
    public static void main(String[] args) {
        String input = "string";
        String output = "summary";
        List<EncodingHelperChar> ehList = null;

        //Sets input and output from the data given
        if (args.length == 1) {
            ehList = createObjectList(args[0], input);
        } else if (args.length == 2) {
            checkTypes(args[0]);
            ehList = createObjectList(args[1], input);
            if (args[0].equals("input")) {
                input = whatType(args[0]);
            } else {
                output = whatType(args[0]);
            }
        } else if (args.length == 3) {
            checkTypes(args[0]);
            checkTypes(args[1]);
            ehList = createObjectList(args[2], input);
            input = whatType(args[0]);
            output = whatType(args[1]);
        } else {
            System.err.println("Usage: java EncodingHelper [--inputtype=type] [--outputtype=type] [data]");
            System.exit(1);
        }

        //if (input.equals("string")) {
        //If the input is one-character...
        if (ehList.size() == 1) {
            if (output.equals("summary")) {
                ehList.get(0).printChSummary();
            } else if (output.equals("string")) {
                System.out.println(getWordFromCP(ehList));
            } else if (output.equals("utf8")) {
                System.out.println("UTF8: " + ehList.get(0).toUTF8StringWithoutQuotes());
            } else if (output.equals("codepoint")) {
                System.out.println("Code point: " + ehList.get(0).toCodePointString());
            }
            //If the input is a multi-character string...
        } else {
            if (output.equals("summary")) {
                String temp = getWordFromCP(ehList);
                printStringSummary(temp, input);
            } else if (output.equals("string")) {
                String temp = getWordFromCP(ehList);
                System.out.println("String: " + temp);
            } else if (output.equals("utf8")) {
                System.out.print("UTF8:");
                for (int i = 0; i < ehList.size(); i++) {
                    System.out.print(" " + ehList.get(i).toUTF8StringWithoutQuotes());
                }
                System.out.println("");
            } else if (output.equals("codepoint")) {
                System.out.println("Code point:");
                for (int i = 0; i < ehList.size(); i++) {
                    System.out.print(" " + ehList.get(i).toCodePointString());
                }
                System.out.println("");
            }
        }
        //End of when inputtype = string
        /*} else if (input.equals("utf8")) {
            if(output.equals("summary")) {
                //Assumes input is one character
                ehList.get(0).printChSummary();
            } else if (output.equals("string")) {
                System.out.println(getWordFromCP(ehList));
            } else if (output.equals("utf8")) {
                System.out.println("UTF8: " + ehList.get(0).toUTF8StringWithoutQuotes());
            } else if (output.equals("codepoint")) {
                System.out.println("Code point: " + ehList.get(0).toCodePointString());
            }
        } else if (input.equals("codepoint")) {

        }*/


        System.out.print("");
        Scanner userInput = new Scanner(System.in);

    }
}