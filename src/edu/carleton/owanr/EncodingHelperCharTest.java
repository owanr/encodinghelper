//Written by Taeyoung Choi and Risako Owan
//CS257 Software Design with Professor Jeff Ondich

package edu.carleton.owanr;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.io.*;
import java.lang.*;


import static org.junit.Assert.*;
public class EncodingHelperCharTest {

    /*Creating objects to use for tests in methods. This is repeated in EncodingHelperChar(),
    but we felt that it was better this way so that we would not have to repeat the process
    in every test method. */
    EncodingHelperChar ch1;
    EncodingHelperChar ch2;
    EncodingHelperChar ch3;
    EncodingHelperChar ch4;
    EncodingHelperChar ch5;
    EncodingHelperChar ch6;

    @Before
    public void setUp() {
        try {
            ch1 = new EncodingHelperChar(12354);
            String s2 = "H";
            byte[] b2 = s2.getBytes("UTF-8");
            ch2 = new EncodingHelperChar(b2);
            ch3 = new EncodingHelperChar('é');
            ch4 = new EncodingHelperChar(12396);
            String s5 = "0";
            byte[] b5 = s5.getBytes("UTF-8");
            ch5 = new EncodingHelperChar(b5);
            ch6 = new EncodingHelperChar('你');
        } catch (UnsupportedEncodingException e) {
            System.out.println("Unsupported Character Set");
        }
    }

    @After
    public void tearDown() {
    }

    @Test
    public void EncodingHelperChar() throws Exception {
        //Creates object from code points
        EncodingHelperChar ch1 = new EncodingHelperChar(12354);
        EncodingHelperChar ch4 = new EncodingHelperChar(12396);
        //Creates object from characters
        EncodingHelperChar ch3 = new EncodingHelperChar('é');
        EncodingHelperChar ch6 = new EncodingHelperChar('你');
        //Creates object from byte arrays
        String s2 = "H";
        byte[] b2 = s2.getBytes("UTF-8");
        EncodingHelperChar ch2 = new EncodingHelperChar(b2);
        String s5 = "0";
        byte[] b5 = s5.getBytes("UTF-8");
        EncodingHelperChar ch5 = new EncodingHelperChar(b5);
    }

    @Test
    public void testGetCodePoint() throws Exception {
        assertEquals(12354,ch1.getCodePoint());
        assertEquals(72,ch2.getCodePoint());
        assertEquals(233,ch3.getCodePoint());
    }

    @Test
    public void testSetCodePoint() throws Exception {
        //ch4's code point is originally 12396
        ch4.setCodePoint(55057);
        assertEquals(55057,ch4.getCodePoint());
        //ch5's code point is originally 48
        ch5.setCodePoint(13579);
        assertEquals(13579,ch5.getCodePoint());
        //ch6's code point is originally 620320
        ch6.setCodePoint(246810);
        assertEquals(246810,ch6.getCodePoint());
    }

    @Test
    public void testToUTF8Bytes() throws Exception {
        byte[] expectedCh1 = new byte[] {(byte)0xe3, (byte)0x81, (byte)0x82};
        byte[] computedCh1 = ch1.toUTF8Bytes();
        assertArrayEquals(expectedCh1, computedCh1);

        byte[] expectedCh2 = new byte[] {(byte)0x48};
        byte[] computedCh2 = ch2.toUTF8Bytes();
        assertArrayEquals(expectedCh2, computedCh2);

        byte[] expectedCh3 = new byte[] {(byte)0xc3, (byte)0xa9};
        byte[] computedCh3 = ch3.toUTF8Bytes();
        assertArrayEquals(expectedCh3, computedCh3);
    }

    @Test
    public void testToCodePointString() throws Exception {
        assertEquals("U+3042",ch1.toCodePointString());
        assertEquals("U+0048",ch2.toCodePointString());
        assertEquals("U+00E9",ch3.toCodePointString());
    }

    @Test
    public void testToUTF8StringWithoutQuotes() throws Exception {
        assertEquals("\\xE3\\x81\\x82",ch1.toUTF8StringWithoutQuotes());
        assertEquals("\\x48",ch2.toUTF8StringWithoutQuotes());
        assertEquals("\\xC3\\xA9",ch3.toUTF8StringWithoutQuotes());
    }

    @Test
    public void testGetCharacterName() throws Exception {
        assertEquals("HIRAGANA LETTER A",ch1.getCharacterName());
        assertEquals("LATIN CAPITAL LETTER H",ch2.getCharacterName());
        assertEquals("LATIN SMALL LETTER E WITH ACUTE",ch3.getCharacterName());
    }

    //Part 2
    @Test
    public void detectInOrOut() throws Exception {
        //Commented out lines that are supposed to return errors
        //ch1.detectInOrOut("--input");
        assertEquals("input",ch1.detectInOrOut("--inputtype ="));
        assertEquals("output",ch1.detectInOrOut("--outputtype ="));
        //ch1.detectInOrOut("--outpu");
        //ch1.detectInOrOut("123");
    }

    @Test
    public void checkTypes() throws Exception {
        //Commented out lines that are supposed to return errors
        ch1.checkTypes("--inputtype=string");
        ch1.checkTypes("--inp ut ty pe =str ing");
        ch1.checkTypes("--inputtype=codepoint");
        ch1.checkTypes("--outpu ttype=s ummary");
        //ch1.checkTypes("--inputtype=summary");
        ch1.checkTypes("--outputtype=utf8");
    }

    @Test
    public void printChSummary() throws Exception {
        ch1.printChSummary();
    }

    @Test
    public void codePointToString() throws Exception {
        System.out.println(ch1.codePointToString(12354));
        System.out.println(ch4.codePointToString(12396));
    }

    @Test
    public void ifUTF8 () throws Exception {
        System.out.println("hello" + ch1.ifUTF8("\\xC3 \\xA9"));
    }

}