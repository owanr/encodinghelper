//Written by Taeyoung Choi and Risako Owan
//CS257 Software Design with Professor Jeff Ondich

package edu.carleton.owanr;

import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * EncodingHelperChar
 *
 * The main support class for the EncodingHelper project in
 * CS 257, Fall 2014, Carleton College. Each object of this class
 * represents a single Unicode character. The class's methods
 * generate various representations of the character.
 */

public class EncodingHelperChar {
    private int codePoint;

    public EncodingHelperChar(int codePoint) {
        this.codePoint = codePoint;
    }

    public EncodingHelperChar(byte[] utf8Bytes) {
        //Initializes and declares local variables.
        String strCodePoint = "";
        Character ch = null;
        try {
            //Converts from UTF-8 byte array to string to character.
            strCodePoint = new String(utf8Bytes, "UTF-8");
            ch = strCodePoint.charAt(0);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //Converts from character to Unicode code point.
        this.codePoint = (int) ch;
    }

    public EncodingHelperChar(char ch) {
        this.codePoint = (int) ch;
    }

    public int getCodePoint() {
        return this.codePoint;
    }

    public void setCodePoint(int codePoint) {
        this.codePoint = codePoint;
    }

    /**
     * Converts this character into an array of the bytes in its UTF-8 representation.
     * For example, if this character is a lower-case letter e with an acute accent,
     * whose UTF-8 form consists of the two-byte sequence C3 A9, then this method returns
     * a byte[] of length 2 containing C3 and A9.
     *
     * @return the UTF-8 byte array for this character
     */
    public byte[] toUTF8Bytes() {
        //Converts code point to string.
        int[] intArray = {(this.codePoint)};
        String toString = new String(intArray, 0, intArray.length);
        byte[] converted = null;
        try {
            //Converts from string to UTF-8 byte array.
            converted = toString.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            System.out.println("Unsupported Character Set");
        }
        return converted;
    }

    /**
     * Generates the conventional 4-digit hexadecimal code point notation for this character.
     * For example, if this character is a lower-case letter e with an acute accent,
     * then this method returns the string U+00E9 (no quotation marks in the returned String).
     *
     * @return the U+ string for this character
     */
    public String toCodePointString() {
        String toHex = Integer.toHexString(this.getCodePoint()).toUpperCase();
        //Ensures that toHex has four digits.
        while (toHex.length() < 4) {
            toHex = "0" + toHex;
        }
        return "U+"+toHex;
    }

    /**
     * Generates a hexadecimal representation of this character suitable for pasting into a
     * string literal in languages that support hexadecimal byte escape sequences in string
     * literals (e.g. C, C++, and Python). For example, if this character is a lower-case
     * letter e with an acute accent (U+00E9), then this method returns the
     * string \xC3\xA9. Note that quotation marks should not be included in the returned String.
     *
     * @return the escaped hexadecimal byte string
     */
    public String toUTF8StringWithoutQuotes() {
        String toUTF8String = "";
        byte[] converted = toUTF8Bytes();
        //Constructs the hexadecimal byte string.
        for (byte b : converted) {
            toUTF8String = toUTF8String+"\\x"+String.format("%x", b).toUpperCase();
        }
        return toUTF8String;
    }

    /**
     * Generates the official Unicode name for this character.
     * For example, if this character is a lower-case letter e with an acute accent,
     * then this method returns the string "LATIN SMALL LETTER E WITH ACUTE" (without quotation marks).
     *
     * @return this character's Unicode name
     */
    public String getCharacterName() {
        String chName = "";
        chName = Character.getName(this.codePoint);
        return chName;
    }

    //Added methods below.

    /**
     * Takes a string and determines whether it represents input ot output.
     * In particular, if the string starts with "--input", the method returns "input".
     * If the string starts with "--output", the method returns "output".
     * Returns an error otherwise
     * @return "input", "output", or error
     */
    public static String detectInOrOut(String string) {
        String[] InputTypes = {"--inputtype=string", "--inputtype=utf8", "--inputtype=codepoint"};
        String[] OutputTypes = {"--outputtype=summary", "--outputtype=string", "--outputtype=utf8", "--outputtype=codepoint"};
        String type = "";
        string = string.replaceAll("\\s","").toLowerCase();
        if (Arrays.asList(InputTypes).contains(string)) {
            type = "input";
        } else if (Arrays.asList(OutputTypes).contains(string)) {
            type = "output";
        } else {
            System.err.println("Usage: java EncodingHelperChar [--inputtype=type] [--outputtype=type] [data]");
            System.exit(1);
        }
        return type;
    }

    /**
     * Takes a code point and converts it to its string representation
     * @return string
     */
    public String codePointToString() {
        String toString = "";
        try {
            toString = new String(toUTF8Bytes(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            System.out.println("Unsupported Character Set");
        }
        return toString;
    }

    /**
     * Prints the summary for a single character.
     */
    public void printChSummary() {
        System.out.println("Character: " + codePointToString());
        System.out.println("Code point: " + toCodePointString());
        System.out.println("Name: " + getCharacterName());
        System.out.println("UTF8: " + toUTF8StringWithoutQuotes());
    }

    /**
     * Takes a string of utf8 bytes and returns the byte[] containing them
     * @param utf8
     * @return byte[]
     */
    public static byte[] ifUTF8 (String utf8) {
        utf8 = utf8.replaceAll("\\s+", "");
        String output="";

        /** Assumes utf8 string to contain \x##
         *  and skips over them to avoid inconvenience.
         */
        byte[] temp = new byte[(utf8.length()/4)];
        for (int i=2; i<utf8.length()-1; i+=4) {
            output = utf8.substring(i, (i + 2));
            int decimal = Integer.parseInt(output, 16);
            temp[(i-2)/4] = (byte) decimal;
        }
        return temp;
    }

    /**
     * Creates and returns a list of EncodingHelperChar objects for words
     * @param string and input
     * @return ArrayList<EncodingHelperChar>
     */
    public static ArrayList<EncodingHelperChar> createObjectList(String string,String input) {
        ArrayList<EncodingHelperChar> ehList = new ArrayList<EncodingHelperChar>();
        if (input.equals("string")) {
            for (int a = 0; a < string.length(); a++) {
                EncodingHelperChar obj = new EncodingHelperChar(string.charAt(a));
                ehList.add(obj);
            }
        } else if (input.equals("utf8")) {
            byte[] givenBytes = ifUTF8(string);
            try{
                String str = new String(givenBytes, "UTF-8");
                for (int f=0; f<str.length(); f++){
                    EncodingHelperChar obj = new EncodingHelperChar(str.charAt(f));
                    ehList.add(obj);
                }
            } catch (UnsupportedEncodingException e) {
                System.out.println("Unsupported Character Set");
            }
        } else {//if codepoint
            String temp = string.replaceAll("\\s+", "");
            String[] codeP = string.split("U+");
            for (int a = 0; a < codeP.length; a++) {
                //Converts hexadecimal codepoints into decimal codepoints
                // Gets rid of empty string before the first "U+".
                if (codeP[a].equals("")) {
                    continue;
                }
                Integer decimal = Integer.parseInt(codeP[a], 16);
                EncodingHelperChar obj = new EncodingHelperChar(decimal);
                ehList.add(obj);
            }
        }
        return ehList;
    }

    /**
     * Takes EncodingHelperChar array list and converts it to a word string
     * @param ehList
     * @return string
     */
    public static String getWordFromCP(ArrayList<EncodingHelperChar> ehList) {
        String string = "";
        for (int i = 0; i < ehList.size(); i++) {
            int[] intArray = {(ehList.get(i).getCodePoint())};
            String toString = new String(intArray, 0, intArray.length);
            string = string + toString;
        }
        return string;
    }

    /**
     * Takes EncodingHelperChar array list and input and prints out the summary
     * @param ehList, input
     */
    public static void printStringSummary(ArrayList<EncodingHelperChar> ehList, String input) {
        System.out.println("String: " + getWordFromCP(ehList));
        System.out.print("Code points: ");
        for (int a = 0; a < ehList.size(); a++) {
            System.out.print(ehList.get(a).toCodePointString() + " ");
        }
        System.out.println("");
        System.out.print("UTF-8: ");
        for (int a = 0; a < ehList.size(); a++) {
            System.out.print(ehList.get(a).toUTF8StringWithoutQuotes());
        }
        System.out.println("");
    }

    /**
     * Returns type given user request
     * @param string
     */
    public static String whatType(String string) {
        string = string.toLowerCase();
        String whatType = "string";
        if(string.endsWith("utf8")) {
            whatType = "utf8";
        } else if (string.endsWith("point")) {
            whatType = "codepoint";
        } else if (string.endsWith("summary")) {
            whatType = "summary";
        }
        return whatType;
    }


    /**
     * Checks that data type matches input
     * @param data, input
     */
    public static void checkDataMatchesInput(String data, String input) {
        if ((input.equals("utf8") && !data.startsWith("\\x")) ||
                (input.equals("codepoint") && !data.startsWith("U+"))){
            System.out.println("Input type does not match data type!");
            System.exit(1);
        }
    }
    /*java EncodingHelperChar [--inputtype=type] [--outputtype=type] [data]
    inputtype can be any of: string [default], utf8, codepoint
    outputtype can be any of: summary [default], string, utf8, codepoint
    If the data argument is present, it is used as the input. Otherwise, input is taken from standard input.
     */
    public static void main(String[] args) {
        //Sets defaults and local variables.
        String input = "string";
        String output = "summary";
        ArrayList<EncodingHelperChar> ehList = null;

        //Sets input and output from the data given.
        //Creates array list for given data.
        if (args.length == 1) {
            ehList = createObjectList(args[0], input);
        } else if (args.length == 2) {
            if (detectInOrOut(args[0]).equals("input")) {
                input = whatType(args[0]);
                checkDataMatchesInput(args[1], input);
            } else if (detectInOrOut(args[0]).equals("output")) {
                output = whatType(args[0]);
            } else {
                System.err.println("Usage: java EncodingHelperChar [--inputtype=type] [--outputtype=type] [data]");
                System.exit(1);
            }
            ehList = createObjectList(args[1], input);
        } else if (args.length == 3) {
            if (detectInOrOut(args[0]).equals("input") && detectInOrOut(args[1]).equals("output")) {
                input = whatType(args[0]);
                output = whatType(args[1]);
                checkDataMatchesInput(args[2], input);
                ehList = createObjectList(args[2], input);
            } else {
                System.err.println("Usage: java EncodingHelperChar [--inputtype=type] [--outputtype=type] [data]");
                System.exit(1);
            }
        } else {
            System.err.println("Usage: java EncodingHelperChar [--inputtype=type] [--outputtype=type] [data]");
            System.exit(1);
        }

        // Returns requested information given output type
        if (output.equals("summary")) {
            if (ehList.size() == 1) {
                ehList.get(0).printChSummary();
            } else {
                printStringSummary(ehList, input);
            }
        } else if (output.equals("string")) {
            System.out.println("String: " + getWordFromCP(ehList));
        } else if (output.equals("utf8")) {
            System.out.print("UTF-8: ");
            for (int i = 0; i < ehList.size(); i++) {
                System.out.print(ehList.get(i).toUTF8StringWithoutQuotes());
            }
            System.out.println("");
        } else if (output.equals("codepoint")) {
            System.out.print("Code point: ");
            for (int i = 0; i < ehList.size(); i++) {
                System.out.print(ehList.get(i).toCodePointString() + " ");
            }
            System.out.println("");
        }
    }
}